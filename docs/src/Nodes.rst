Nodes
============================
Here follows the description of all the modification made for every node in order to fit to our jetbot.

Sensor Transforms
-----------------------
For the Jetbot to drive on a map, the local coordinate system of the Jetbot is transformed to the global coordinate system. That way it is possible to know where the Jetbot is and where it is going on the map. In order to create the transformation between each frame, the :code:`tf` package already built inside of ROS can be used and put inside the launch file, in this case they have been added to the **start_mapping.launch** and **jetbot_configuration.launch** files.


- The map reference and the odometry one, called 'odom', overlaps beacuse they are the world reference of the robot; for this reason the transformation defined below is **0 0 0 0 0 0**:  
:code:`<node pkg="tf" type="static_transform_publisher" name="map_to_odom" args="0 0 0 0 0 0 map odom 30" />`  

- The camera_link reference is linked to base_link one, that is the local reference, which is shifted 0.06 m in the x-axis and 0.08m in the z-axis, because the camera is located in the front of the Jetbot, so:  
:code:`<node pkg="tf" type="static_transform_publisher" name="base_link_to_camera" args="0.06 0 0.08 0 0 0 base_link camera_link 30" />`    

- The laser reference, creted by the pointcloud_to_laserscan node overlap with the camera_link reference:  
:code:`<node pkg="tf" type="static_transform_publisher" name="camera_to_laser" args="0 0 0 0 0 0 camera_link laser 30" />`  

All frames are linked as shown below:  

.. figure:: ../figs/ros/frames-1.png

An example of all transformation, taken from the **start_mapping.launch** file, is shown below:

.. literalinclude:: ../../src/8_start_mapping.launch
    :language: python



Odometry Source
-----------------------
The odometry is to measure the motion from the encoders connected to the wheel to estimate the position of the Jetbot. To measure the angular velocity of the wheels over a small periode of time, we can calculate the position using the following equations: 

.. math::
    \omega = \frac{r}{2 b} \cdot ((\alpha R_{new} - \alpha R_{old}) - (\alpha L_{new} - \alpha L_{old})) \\
    \Delta \theta = \omega \cdot \Delta t \\
    \theta = \theta + \Delta \theta \\
    v_x = cos(\theta) \cdot v \\
    v_y = sin(\theta) \cdot v \\
    \Delta x = v_x \cdot \Delta t \\
    \Delta y = v_y \cdot \Delta t \\
    x = x + \Delta x \\
    y = y + \Delta y
    

Inside the code of EncoderReader will look like this

.. literalinclude:: ../../src/8_EncoderReader.py
    :language: python


Setting them into a python code where the angular velocity of the wheels are measured. As noticeable, the encoder node is subscribeed to the **encoder** topic created by the Arduino node (shown below), in which the string message contains the angle left and angle right values, in the first and second position respectively. Then the angles are saved into global variables, so they can be processed by the main code.  
Here the code solve the problems related to the saturation of the variables; the angles coming from Arduino is constantly increasing while the jetbot is running, so when the angle reaches approx 32000 reaches the highest representable number possible, it will make a jump to -32000. During this jump the code detects a very high variation of angle that is translated in a high velocity, even if the robot is moving with the same speed. To solve this, an if statement has been implemented: when the absolute value of the difference between the actual angle and the previous one is higher than 3000, the code will skip one cycle without updating the jetbot speed.  
Inside the while loop the forward kinematics is implemented, using the equations shown above, to obtain the x and theta coordinates. In the end, using the broadcast transformer, the base_link coordinates are transformed into the odom coordinates, then to global reference.

************
Arduino
************
The Arduino is used for reading the encoder of both wheels and sending the angle measures to a node, called Serial_arduino, through the serial port. 
Using the rosserial_arduino package, that can be installed by writing this in a terminal:  
 
   * ``sudo apt-get install ros-melodic-rosserial-arduino``  
   * ``sudo apt-get install ros-melodic-rosserial``  
Then importing the ROS library into Arduino. Doing these steps it is possible to create a ROS node inside of Arduino.  

.. literalinclude:: ../../Arduino/Read_encoder.ino
    :language: python

    
As shown in the code above, code inside the *setup* statement, the ROS node is initialed and the interrupts are defined; in fact the Arduino use interrupts signal from pin 2 and 3 to read the encoder signal. The interrupts enable a precise function for each wheel in which the angle in calcualted.
In order to reduce the noise coming from the encoder, a double low pass filter is implemented with a gain of 0.98 : 

 * ``angle_left1 = fGain*angle_left1 + (1-fGain)*angle_left;`` 
 * ``angle_left2 = fGain*angle_left2 + (1-fGain)*angle_left1;``
 * ``angle_left= angle_left2;``

Then in the **loop** statement, the filtered angles are converted into chars and attached to the same message ready to be sent into the Encoder topic.  

Base Controller
-----------------------
For the base controller, we have the InverseKinematics node that receive the data from the Navigation Stack in a **Twist** message, in which the desired velocity and the yaw is defined. It then calculates the needed angular velocity on both wheels as shown below, and it send the speed data to the **JetbotController** node.

.. literalinclude:: ../../src/8_InverseKinematics.py
    :language: python

The **JetbotController** node, process the speed data and send it to the Adafruit board, then correct the PWM reference signal to set the desired velocity.

.. literalinclude:: ../../src/8_JetbotController.py
    :language: python

Sensor Sources
-----------------------
To see where the Jetbot is going, a Realsense Lidar Camera is used to map the environment. It is composed by a depth and a RGB camera to acquire data and the distance. Thanks to the realsense repository the camera is able to create a pointcloud of the acquired image; then using a pointcloud to laserscan node iy is possible to make this transformation, because the navigation needs a laserscan. 
The installation steps for the RealSense2 are based on:   
https://github.com/IntelRealSense/librealsense/blob/master/doc/installation_jetson.md.

From this package only the ``rs_camera.launch`` file will be used and some modification has been done:

   * ``<arg name="enable_color"        default="false"/>``
   * ``<arg name="pointcloud_texture_stream" default="RS2_STREAM_ANY"/>``
   
 With these changes, we remove the stream color from the camera and from the point cloud in order to make it faster to process images.  

Instead, of installing the pointcloud to laserscan node can be followed: 
http://wiki.ros.org/pointcloud_to_laserscan   
Some changes are needed in the ``sample_node.launch`` file located inside the launch folder of the **pointcloud_to_laserscan** package : 
 
   * ``<remap from="scan" to="scan"/>``
   * ``scan_time: 0.03333``
   * ``concurrency_level: 2``
The first modification is mandatory, beacuse the navigation stack is subscribed to a **scan** topic, in which all the data should come. By default, the laserscan publich data on the ``/camera/scan`` topic, so a change was needed.  
Every node in the roscore must works with approximately the same frequency, so we set a 30 Hz frequency.  
In the end, the concurrency level is set to 2, in order to enable the largest number of threads to procces image data, so it can be faster.




