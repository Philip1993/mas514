Navigation Stack Setup
============================
Autonomous driving is based on the navigation stack, which is composed by differents nodes and parts as shown below:

.. figure:: ../figs/ros/navigation_stack.png

The blue boxes are the nodes that must be created, and in the *Nodes* chapter each node will be described step by step. The light grey boxes are the nodes already present inside of navigation stack, and that require only to be properly tuned; this operation will be described in the following sections.


Creating a Package
-----------------------
The ROS will require configuration and launch files for the navigation stack. By creating a package for this will fulfill the requirements, and allow navigation. Pick a location on the jetbot and run the command: 

    * ``"catkin_create_pkg jetbot_2dnav move_base my_tf_configuration_dep my_odom_configuration_dep my_sensor_configuration_dep"``
This creates a package with the necessary dependencies for the navigation stack on the Jetbot. 

This will be the workspace for the Jetbots configurations and launch files. Then to create a roslaunch file that will be used to bring all the hardware and transform published needed to drive and navigate the Jetbot. Inside a editor named jetbot_configuration.launch write the following: 

.. literalinclude:: ../../src/8_jetbot_configuration.launch.py
    :language: python

As noticeable, in the first part are implemented the transformation between each frame used by the our nodes(for better understanding see Transformation section on the *Nodes* chapter). Then follow the activation commands for the motion and sensing nodes, in particular all the blue nodes shown in the picture above. They activate the encoder reading and the serial communication with Arduino in order to create the **odom** message requested by the navigation stack. Then, the inverse kinematics and jetbot controllers, transforms the messages going from the navigation stack into speed commands for the wheels. In the end, the node for the camera is activated so the jetbot can enable obstacle avoidance.  

Costmap Configuration (local_costmap) & (global_costmap)
----------------------------------------------------------


The navigation stack needs two configuration files called "local_costmap" and "global_costmap". One global that define the long-term parameters for path, and one local for avoiding obstacles that can appear in front of the robot and that are not present in the map. 

Create a file called costmap_common_params.yaml and write the following:

.. literalinclude:: ../../src/8_costmap_common_params.yaml.py
    :language: python

It is important to use [m] and not in [cm] nor [mm] for the costmap file, or else the map will be all wrong.  
Our system does not use the pointcloud for detecting obstacles but only the laserscan, so the **pointcloud sensor**  definition has been comment out and on the **laser_scan sesnro** definition is mandatory to change the name of the topic into **/scan**.

Base Local Planner Configuration
-----------------------------------
Then a file named: base_local_planner_params.yaml has been created, which describe other parameters for the local map:

.. literalinclude:: ../../src/8_base_local_planner_params.py
    :language: python
In this file is necessary to change to **holonomic_robot: false** because we are implementing the navigation stack on a differential drive robot which is a nonholonomic robot.  
The maximum velocity and accelleration values has been reduced in order to move the robot with a lower speed, giving time to the laserscan to detect new obstacles and calculating a new path.  
Then the move_base.launch file has been created:

.. literalinclude:: ../../src/8_move_base.launch.py
    :language: python

In this file it is presumed that the map is already built inside of the jetbot, because it is presumed that before starting the autonomous navigation, the user has followed the *Mapping* part of the instructions. Then, include all the parameter files previously described, and also the amcl_diff launch file. This file defines the other parameters for navigation, in particular for a differential robot.


    
