Launch
============================

Described here are how to make a map and autonomous driving for the jetbot.  
It is neccesary to have installed ROS melodic full desktop version and Arduino on the board.

Start
-----------------------

Setup of the jetbot:  

- Connect the jetbot to a screen, mouse and keyboard.
- Connect the Intel L515 Camera.  
- Connect the Arduino nano.
- Connect the jetbot to a power supply and it will automatically turn on.
- Enter the password :code:`jetbot` on the start screen. 
- Connect to internet hotspot from your laptop or connect with ethernet cable. 
- Download the folder called **catkin_ws** from git (https://gitlab.com/Philip1993/mas514/-/tree/A2021/catkin_ws).  
- Open a new terminal
    - Set the location to the catkin_ws folder by writing :code:`cd catkin_ws`.
    - Type :code:`catkin_make` to start the program.
- Copy the file **librealsense2_camera.so** located inside the catkin_ws folder and copy it to **catkin_ws/devel/lib**.
- Using Arduino software:  select the **/dev/ttyUSB0** port in the ''tools'' menu; then  upload the code **Read_Encoder** that you can find at the path **catkin_ws/Arduino** into the Arduino board. 

Mapping
--------------

- In the same terminal:
    - Type :code:`roslaunch mas514 start_mapping.launch` to launch the mapping file. 
- Open a new terminal:
    - Type :code:`roslaunch jetbot_key jetbot_key.launch` to launch the keyboard.
- Open a new terminal:
    - Type :code:`roslaunch gmapping slam_gmapping_pr2.launch` to launch the mapping.
- Open a new terminal:
    - Type :code:`rviz`.
- Wait for rviz to open:
    - Inside rviz add the display called **map** and choose the topic **/map** to visualize building up the map. 
    - Use the keyboard to move the jetbot (W=forward, D=right, A=left, S=stop and X=backward).
- When the map is ready, open a new terminal.
    - Type :code:`rosrun map_server map_saver -f my_map` to save the map.   
    
Type **Ctrl+c** in each terminal to cancel the process. 

Autonomous Driving
---------

- Open a new terminal:
    - Type :code:`roslaunch jetbot jetbot_configuration.launch`.
- Open a new terminal:
    - Type :code:`roslaunch jetbot move_base.launch`.
- Open a new terminal:
    - Type :code:`rviz` to open rviz. 
- In rviz:
    - Add the display called **map** and select the topic **/map**.
    - Add the display called **TF** to see the robot on the map.
    - Add the display called **Path**, and select the topic **/move_base/NavfnROS/plan** to see the path the robot is calculating in order to reach the destination point.
    - Press the button called **2D Pose Estimate**, click and drag the green arrow to define the initial position.
    - Press the button called **2D Nav Goal**, click and drag the pink arrow to define the destination point. 
 

When finished, type **Ctrl+c** in each terminal to cancel the process. 






