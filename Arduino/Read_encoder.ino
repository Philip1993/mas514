/*
* ------------ MAS514 ---------------------------------------------------------------
* Wheel Encoder and Arduino 
* Read Encoder values and publish them in ROS
*/

// Includes
#include <ros.h>
#include <string.h>
#include <std_msgs/String.h>

//Variables of encoder reading
static volatile long int angle_left, angle_left1, angle_left2;
static volatile long int angle_right, angle_right1, angle_right2;
static int thetaPre_left=0;
static int thetaPre_right=0;
static int thetaPrePre_left=0;
static int thetaPrePre_right=0;
static unsigned int tHigh_left, tLow_left;
static unsigned long rise_left, fall_left;
static unsigned int tHigh_right, tLow_right;
static unsigned long rise_right, fall_right;
static int turns_left;
static int turns_right;
// Parameters
static const int unitsFC = 360;
static const float dcMin = 0.029;
static const float dcMax = 0.971;
static const int dutyScale = 1;
static const int q2min = unitsFC / 4;
static const int q3max = q2min * 3;
static const float alpha=0.4;
static const float beta=0.2;
static const float gamma=0.4;
// Variables for publisher
char*message=(char*)malloc(30 *sizeof(char));
char*convertedChar = (char*)malloc(6 * sizeof(char)); 
std_msgs::String str_msg;
ros::NodeHandle  nh;    
ros::Publisher encoder("encoder", &str_msg);



static const float fGain = 0.98;
// function to add data to messagePointer in CSV form
void attach(char* messagePointer, int data)
{
  char comma[2] = ",";
  sprintf(convertedChar, "%d", data);
  strcat(messagePointer,comma);
  strcat(messagePointer,convertedChar);
  return;
}


void setup() {
  
  //set up node for sending encoder data
  nh.getHardware()->setBaud(57600);
  nh.initNode();
  nh.advertise(encoder);
  // Interrupt pin #2
  attachInterrupt(0, feedback_left, CHANGE);  
  // Interrupt pin #3
  attachInterrupt(1, feedback_right, CHANGE);  

}

void loop() {


  // Attach first data value to message
  sprintf(convertedChar, "%d", angle_left);
  strcpy(message, "\0");
  strcat(message,convertedChar);
  // Attach other data values to message as CSV-message
  attach(message, angle_right);
  // Publish the message
  str_msg.data=message;
  encoder.publish(&str_msg);
  nh.spinOnce();
  //Serial.println(message); 
  delay(30);

}
 // Function for reading left wheel angle and number of turns
void feedback_left() {
 
 if (digitalRead(2)) {
    rise_left = micros();
    tLow_left = rise_left - fall_left;

    int tCycle_left = tHigh_left + tLow_left;
    if ((tCycle_left < 1000) || (tCycle_left > 1200))
      return;

    float dc_left = (dutyScale * tHigh_left) / (float)tCycle_left;
    float theta_left = ((dc_left - dcMin) * unitsFC) / (dcMax - dcMin);

    if (theta_left < 0.0)
      theta_left = 0.0;
    else if (theta_left > (unitsFC - 1.0))
      theta_left = unitsFC - 1.0;

    if ((theta_left < q2min) && (thetaPre_left > q3max))
      turns_left++;
    else if ((thetaPre_left < q2min) && (theta_left > q3max))
      turns_left--;

    if (turns_left >= 0){
      angle_left = (turns_left * unitsFC) + theta_left;
    }
    
    else if (turns_left < 0){
      angle_left = ((turns_left + 1) * unitsFC) - (unitsFC - theta_left);
    }

    thetaPre_left = theta_left;
  } else {
    fall_left = micros();
    tHigh_left = fall_left - rise_left;
  }
  

    angle_left1 = fGain*angle_left1 + (1-fGain)*angle_left;
    angle_left2 = fGain*angle_left2 + (1-fGain)*angle_left1;
    angle_left= angle_left2; 
  

}

 // Function for reading right wheel angle and number of turns
void feedback_right() {
  
 if (digitalRead(3)) {
    rise_right = micros();
    tLow_right = rise_right - fall_right;

    int tCycle_right = tHigh_right + tLow_right;
    if ((tCycle_right < 1000) || (tCycle_right > 1200))
      return;

    float dc_right = (dutyScale * tHigh_right) / (float)tCycle_right;
    float theta_right = ((dc_right - dcMin) * unitsFC) / (dcMax - dcMin);

    if (theta_right < 0.0)
      theta_right = 0.0;
    else if (theta_right > (unitsFC - 1.0))
      theta_right = unitsFC - 1.0;

    if ((theta_right < q2min) && (thetaPre_right > q3max))
      turns_right++;
    else if ((thetaPre_right < q2min) && (theta_right > q3max))
      turns_right--;

    if (turns_right >= 0){
      angle_right = (turns_right * unitsFC) + theta_right;
    }
    else if (turns_right < 0){
      angle_right = ((turns_right + 1) * unitsFC) - (unitsFC - theta_right);
    }

    thetaPre_right = theta_right;
  } else {
    fall_right = micros();
    tHigh_right = fall_right - rise_right;
  }
    angle_right1 = fGain*angle_right1 + (1-fGain)*angle_right;
    angle_right2 = fGain*angle_right2 + (1-fGain)*angle_right1;
    angle_right = angle_right2;

}
