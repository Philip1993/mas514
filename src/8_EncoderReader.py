#!/usr/bin/env python
import rospy
import tf
import math
from math import sin, cos, pi
from std_msgs.msg import String
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Vector3

angle_left=1.0
angle_right=1.0

def get_encoder_data(data):  
    # Cast and store received data
    global angle_left
    global angle_right
    angle_right=float(data.data.split(",")[0])
    angle_left=float(data.data.split(",")[1])
    

if __name__ == '__main__':
    # init node
    rospy.init_node('odometry_publisher', anonymous=True)
    
    # set up publishers 
    odom_pub = rospy.Publisher("odom", Odometry, queue_size=50)
    odom_broadcaster = tf.TransformBroadcaster()

    # subscribe to encoder
    rospy.Subscriber("encoder", String, get_encoder_data)
    
    # variables
    current_time = rospy.Time.now()
    last_time = rospy.Time.now()
    
    pi = 3.141592653589793
    
    r = 0.03
    b = 0.08
    
    v =0.0
    v_x = 0.0
    v_y = 0.0
    v_theta=0.0
    x = 0.0
    y = 0.0
    theta = 0.0
    
    angle_left_new = 0.0
    angle_right_new = 0.0
    angle_left_old = 0.0
    angle_right_old = 0.0

    rate = rospy.Rate(30)
    while not rospy.is_shutdown():

        # get current time and angle data
        current_time = rospy.Time.now()
        angle_right_new=-angle_right * pi/180
        angle_left_new=angle_left * pi/180
        rospy.loginfo("AL: " + str(angle_left) +" AR: " + str(angle_right))

	if abs(angle_right_new - angle_right_old) >3000: 
	    angle_right_old = angle_right_new
	    continue
	if abs(angle_left_new - angle_left_old) >3000: 
	    angle_left_old = angle_left_new
	    continue

        #calculate changes in tf
        dt = (current_time - last_time).to_sec()
        v_theta = -r/(2*b)*((angle_right_new-angle_right_old)-(angle_left_new-angle_left_old))/dt
        delta_theta = v_theta*dt
        theta+=delta_theta
        
        v = r/2 * ((angle_right_new-angle_right_old)+(angle_left_new-angle_left_old))/dt
        v_x = cos(theta)*v
        v_y = sin(theta)*v
        delta_x = v_x*dt
        delta_y = v_y*dt

        x += delta_x
        y += delta_y

        # since all odometry is 6DOF we'll need a quaternion created from yaw
        odom_quat = tf.transformations.quaternion_from_euler(0, 0, theta)

        #broadcast transform
        odom_broadcaster.sendTransform(
            (x, y, 0.),
            odom_quat,
            current_time,
            "base_link",
            "odom"
        )

        # next, we'll publish the odometry message over ROS
        odom = Odometry()
        odom.header.stamp = current_time
        odom.header.frame_id = "odom"

        # set the position
        odom.pose.pose = Pose(Point(x, y, 0.), Quaternion(*odom_quat))

        # set the velocity
        odom.child_frame_id = "base_link"
        odom.twist.twist = Twist(Vector3(v, 0, 0), Vector3(0, 0, v_theta))

        # publish the message
        odom_pub.publish(odom)

        angle_right_old = angle_right_new
        angle_left_old = angle_left_new
        last_time = rospy.Time.now()

        #keep programm running
        rate.sleep()
