<launch>

   <master auto="start"/>
 <!-- Run the map server --> 
    <node name="map_server" pkg="map_server" type="map_server" args="/home/jetbot/map.yaml"/>

 <!--- Run AMCL --> 
    <include file="/home/jetbot/catkin_ws/src/jetbot/launch/amcl_diff.launch" />

   <node pkg="move_base" type="move_base" respawn="false" name="move_base" output="screen">
    <rosparam file="/home/jetbot/catkin_ws/src/jetbot/params/costmap_common_params.yaml" command="load" ns="global_costmap" /> 
    <rosparam file="/home/jetbot/catkin_ws/src/jetbot/params/costmap_common_params.yaml" command="load" ns="local_costmap" />
    <rosparam file="/home/jetbot/catkin_ws/src/jetbot/params/local_costmap_params.yaml" command="load" />
    <rosparam file="/home/jetbot/catkin_ws/src/jetbot/params/global_costmap_params.yaml" command="load" /> 
    <rosparam file="/home/jetbot/catkin_ws/src/jetbot/params/base_local_planner_params.yaml" command="load" />
 </node>

</launch> 
