<launch>
    <!-- Transformation Configuration ... Setting Up the Relationships Between Coordinate Frames --> 
    <node pkg="tf" type="static_transform_publisher" name="map_to_odom" args="0 0 0 0 0 0 1 map odom 30" />
    <node pkg="tf" type="static_transform_publisher" name="base_link_to_camera" args="0.06 0 0.08 0 0 0  base_link camera_link 30" />
    <node pkg="tf" type="static_transform_publisher" name="camera_to_laser" args="0 0 0 0 0 0 camera_link laser 30" />
    <node pkg="tf" type="static_transform_publisher" name="map_to_laser" args="0.06 0 0.08 0 0 0 map laser 30" />


    <node name="JetbotController" pkg="mas514" type="JetbotController.py" output="screen"/>
    <node name="EncoderReader" pkg="mas514" type="EncoderReader.py" output="screen"/>
    <node name="InverseKinematics" pkg="mas514" type="InverseKinematics.py" output="screen"/>
    <node pkg="rosserial_python" type="serial_node.py" name="serial_node">
        <param name="port" value="/dev/ttyUSB0"/>
        <param name="baud" value="57600"/>
     </node> 
     
    <include file="$(find realsense2_camera)/launch/rs_camera2.launch"/>
    <include file="$(find pointcloud_to_laserscan)/launch/sample_node.launch"/>
</launch> 
